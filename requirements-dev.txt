# Runtime requirements
--requirement requirements.txt

# Testing
pytest==2.7.1
py==1.4.28
mock==1.0.1
httpretty==0.8.10

# Linting
flake8==2.4.1
mccabe==0.3.1
pep8==1.6.2
pyflakes==0.9.2

# Documentation
Sphinx==1.3.1
six==1.9.0
Babel==1.3
pytz==2015.4
alabaster==0.7.5
sphinx-rtd-theme==0.1.8
docutils==0.12
Jinja2==2.7.3
MarkupSafe==0.23
Pygments==2.0.2

# Miscellaneous
Paver==1.2.4
colorama==0.3.3

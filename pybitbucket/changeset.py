from functools import partial

from uritemplate import expand

from pybitbucket.bitbucket import BitbucketBase, Client
from pybitbucket.user import User
from pybitbucket.repository import Repository


class Changeset(BitbucketBase):
    id_attribute = 'hash'

    # Must override base constructor to account for approve and unapprove
    def __init__(self, data, client=Client()):
        self.data = data
        self.client = client
        self.__dict__.update(data)
        for link, body in data['links'].items():
            if link == 'clone':
                self.clone = {item['name']: item['href'] for item in body}
            elif link == 'approve':
                setattr(
                    self,
                    'approve',
                    partial(self.post_commit_approval, url=body['href']))
                setattr(
                    self,
                    'unapprove',
                    partial(self.delete_commit_approval, url=body['href']))
            else:
                for head, url in body.items():
                    setattr(
                        self,
                        link,
                        partial(self.client.remote_relationship, url=url))
        if self.data.get('author'):
            self.raw_author = self.data['author']['raw']
            self.author = User(
                self.data['author']['user'],
                client=client)
        if self.data.get('repository'):
            self.repository = Repository(
                self.data['repository'],
                client=client)

    @staticmethod
    def comment_on_changeset_in_repository_by_id(
            username,
            repository_name,
            id,
            comment,
            client=Client()):
        template = (
            'https://bitbucket.org/api/1.0/repositories/{username}/{repository_name}/changesets/{id}/comments')
        url = expand(
            template,
            {
                'bitbucket_url': client.get_bitbucket_url(),
                'username': username,
                'repository_name': repository_name,
                'id': id,
            })
        for changeset in client.remote_relationship(url, comment):
            yield changeset

    @staticmethod
    def get_a_changeset_by_id(
            username,
            repository_name,
            id,
            client=Client()):
        template = (
            'https://bitbucket.org/api/1.0/repositories/{username}/{repository_name}/changesets/{id}')
        url = expand(
            template,
            {
                'bitbucket_url': client.get_bitbucket_url(),
                'username': username,
                'repository_name': repository_name,
                'id': id,
            })
        for changeset in client.remote_relationship(url):
            yield changeset

    @staticmethod
    def is_type(data):
        return data.get('hash')

Client.bitbucket_types.add(Changeset)
